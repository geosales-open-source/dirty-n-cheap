#!/bin/bash

import_stuff() {
	if [ -f "${cur_dir:+$cur_dir/}lib.sh" ]; then
		. "${cur_dir:+$cur_dir/}lib.sh"
	fi
}

instalar_exec() {
	local conf_dir exec_var cur
	local rc_file=~/.bash_profile
	while [ $# -gt 0 ]; do
		case "$1" in
			-c|--conf)
				shift
				conf_dir="$1"
				;;
			-d)
				shift
				cur="$1"
				;;
			-e)
				shift
				exec_var="$1"
				;;
		esac
		shift
	done

	local dir_alvo

	read -p "Qual o diretório de instalação? (padrão é ~/bin) " dir_alvo

	dir_alvo="$(sed "s_^~/_$HOME/_" <<<"${dir_alvo}")"
	dir_alvo="${dir_alvo:-$HOME/bin}"

	mkdir -p "$dir_alvo"

	if confirmacao "Deseja colocar o diretório $dir_alvo no PATH?"; then
		cat > "$rc_file" << EOL
PATH="\${PATH:+\$PATH:}$dir_alvo"
export PATH
EOL
	fi

	mkdir -p ~/etc/template-generator/templates

	cp -v "$conf_dir/templates/add" ~/etc/template-generator/templates/add
	cp -v "${cur_dir}/lib.sh" ~/etc/template-generator/
	cp -v "$cur/${exec_var}" "$dir_alvo"
	"$cur/${exec_var}" add -s "$conf_dir/templates/template-test" -n template-test
}

NOME_EXECUTAVEL=template-generator
cur_dir=${BASH_SOURCE[0]%/*}
template_dir=$cur_dir/template-dir
CONF_DIR=$template_dir/etc/template-generator

import_stuff

template_generator=`which ${NOME_EXECUTAVEL} 2>/dev/null`

echo $cur_dir
echo $template_generator

if [ -z "$NOME_EXECUTAVEL" ]; then
	abort "Não foi encontrado o executável \"$NOME_EXECUTAVEL\""
elif confirmacao "Deseja instalar agora?"; then
	instalar_exec -d "$template_dir" -e "$NOME_EXECUTAVEL" --conf "$CONF_DIR"
fi
