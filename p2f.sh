#!/bin/bash

abort() {
	echo "$@" >&2
	exit 1
}

abort_faltando_complemento() {
	abort "Esperava algum complemento para a opção '${1%%=*}', nenhum encontrado"
}

check_variavel_obrigatoria() {
	local var="$1"
	if [ -z "${!var}" ]; then
		abort "É necessário setar o valor de ${var}"
	fi
}

check_variable_is_type() {
	local VAR_NAME="$1"
	local VAR_TYPE="$2"
	declare -p "$1" 2>/dev/null | grep "^declare $2 $1" -q
}

check_variable_is_map() {
	check_variable_is_type "$1" -A
}

check_variable_is_array() {
	check_variable_is_type "$1" -a
}

variaveis_globais() {
	local branch mnemonic mnemonic_base branch2 cnt
	# algumas variáveis globais...

	if [ -z "$MAIN_BRANCH" ]; then
		MAIN_BRANCH=main
	fi

	if [ -z "$RELEASE_BRANCH" ]; then
		RELEASE_BRANCH=release-candidate
	fi

	if [ -z "$DEVELOPMENT_BRANCH" ]; then
		DEVELOPMENT_BRANCH=develop
	fi

	if ! check_variable_is_array BRANCH_LIFECYCLE; then
		declare -ag BRANCH_LIFECYCLE
		BRANCH_LIFECYCLE[0]=$MAIN_BRANCH
		BRANCH_LIFECYCLE[1]=$RELEASE_BRANCH
		BRANCH_LIFECYCLE[2]=$DEVELOPMENT_BRANCH
	fi

	if ! check_variable_is_map BRANCH_MNEMONICS; then
		# não existe, portanto vamos criar
		declare -Ag BRANCH_MNEMONICS
		for branch in "${BRANCH_LIFECYCLE[@]}"; do
			mnemonic_base=${branch::1}
			mnemonic="$mnemonic_base"
			cnt=1
			while [ -n "${BRANCH_MNEMONICS[${mnemonic}]}" ]; do
				cnt=$(( cnt + 1 ))
				mnemonic="$mnemonic_base$cnt"
			done
			BRANCH_MNEMONICS[${mnemonic}]=$branch
		done
	fi

	declare -Ag REVERSE_BRANCH_MNEMONICS

	for mnemonic in "${!BRANCH_MNEMONICS[@]}"; do
		REVERSE_BRANCH_MNEMONICS["${BRANCH_MNEMONICS["$mnemonic"]}"]=$mnemonic
	done

	for branch in "${BRANCH_LIFECYCLE[@]}"; do
		mnemonic="${REVERSE_BRANCH_MNEMONICS[$branch]}"
		if [ -z "$branch" ]; then
			abort "Branch do ciclo de vida com nome vazio"
		elif [ -z "$mnemonic" ]; then
			abort "Mnemônico do branch '$branch' vazio"
		fi
		branch2="${BRANCH_MNEMONICS["$mnemonic"]}"
		if [ "$branch" != "$branch2" ]; then
			abort "Mnemônico '$mnemonic' do '$branch' apotando também para '$branch2'"
		fi
	done

	declare -p REVERSE_BRANCH_MNEMONICS BRANCH_MNEMONICS BRANCH_LIFECYCLE

	local var
	for var in PROJECT_ID GITLAB_SERVER REMOTE_REPO PRIVATE_TOKEN; do
		check_variavel_obrigatoria "$var"
	done
}

existe_diff() {
	local passado=$1
	local futuro=$2
	[[ `git log --no-decorate -1 --format=oneline ${passado} ^${futuro} | wc -l` > 0 ]]
}

verifica_alem_futuro() {
	local branch_base="$1"
	local -i idx_base="$2"
	local -i idx_max="$3"

	for i in `seq $(( idx_base + 1 )) $idx_max`; do
		i=$(( i - 1 ))
		if ${BRANCH_EXISTS[$i]}; then
			branch_lifecycle_name="${BRANCH_LIFECYCLE[$i]}"
			if existe_diff "$REMOTE_REPO/$branch_base" "$REMOTE_REPO/$branch_lifecycle_name"; then
				SOURCE_SOURCE_BRANCH="$REMOTE_REPO/$branch_base"
				SOURCE_PUSHED_BRANCH="${REVERSE_BRANCH_MNEMONICS[$branch_base]}2${REVERSE_BRANCH_MNEMONICS[$branch_lifecycle_name]}"
				TARGET_BRANCH="$branch_lifecycle_name"
				DO_DETECT_BRANCH=false
				return
			else
				return 1
			fi
		fi
	done
	return 1
}

mnemonico_real() {
	local candidato="${BRANCH_MNEMONICS[$1]}"
	if [ "z$candidato" != z ];then
		echo "$candidato"
	else
		echo "$1"
	fi
}

show_help() {
	cat <<EOF
Texto de help
EOF
}

DO_DETECT_BRANCH=true
CLI_BRANCH=false
CLI_YES=false
DRY_RUN=false
CLI_FROM=
CLI_TARGET=
REMOTE_REPO=origin
GITLAB_SERVER=https://gitlab.com

# tratar argumentos CLI
while [ $# -gt 0 ]; do
	case "$1" in
		-f*)
			CLI_FROM="${1#-f}"
			#SOURCE_SOURCE_BRANCH=origin/`mnemonico_real "${1#-f}"`
			CLI_BRANCH=true
			;;
		-f)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			CLI_FROM="${1}"
			#SOURCE_SOURCE_BRANCH=`mnemonico_real "${1}"`
			CLI_BRANCH=true
			;;
		-2*)
			CLI_TARGET="${1#-2}"
			#TARGET_BRANCH=`mnemonico_real "${1#-2}"`
			CLI_BRANCH=true
			;;
		-2)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			CLI_TARGET="${1}"
			#TARGET_BRANCH=`mnemonico_real "${1}"`
			CLI_BRANCH=true
			;;
		--as)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			SOURCE_PUSHED_BRANCH="$1"
			CLI_BRANCH=true
			;;
		--main-branch)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			MAIN_BRANCH="$1"
			;;
		--release-branch)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			RELEASE_BRANCH="$1"
			;;
		--development-branch)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			DEVELOPMENT_BRANCH="$1"
			;;
		--remote)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			REMOTE_REPO="$1"
			;;
		--gitlab-server)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			GITLAB_SERVER="$1"
			;;
		-y|--yes)
			CLI_YES=true
			;;
		-n|--dry-run)
			DRY_RUN=true
			;;
		--token)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			PRIVATE_TOKEN="$1"
			;;
		--project-id)
			if [ $# -eq 1 ]; then
				abort_faltando_complemento "$1"
			fi
			shift
			PROJECT_ID=$1
			;;
		--help)
			show_help
			exit 0
			;;
	esac
	shift
done

variaveis_globais
declare -p REVERSE_BRANCH_MNEMONICS BRANCH_MNEMONICS BRANCH_LIFECYCLE

if [ -n "$CLI_FROM" ]; then
	SOURCE_SOURCE_BRANCH="$REMOTE_REPO"/`mnemonico_real "${CLI_FROM}"`
fi

if [ -n "$CLI_TARGET" ]; then
	TARGET_BRANCH=`mnemonico_real "${CLI_TARGET}"`
fi

if ${CLI_BRANCH}; then
	if [ -z "${SOURCE_SOURCE_BRANCH}${TARGET_BRANCH}" ]; then
		echo "Já que vai informar branches, informe a origem [-f] e o destino [-2]" >&2
	fi
	
	if [ -z "${SOURCE_PUSHED_BRANCH}" ]; then
		echo "Usando p2f como o alias [--as]" >&2
		SOURCE_PUSHED_BRANCH=p2f
	fi
	DO_DETECT_BRANCH=false
fi

# se ainda mantém a questão da detecção automática, então seto o valor adequado

if ${DO_DETECT_BRANCH}; then
	if ! $CLI_YES; then
		read -p "Será feita a autodetecção do branch a ser enviado; para tal, vamos fazer prune, tudo bem? [Sn] " confirma
	else
		echo "Fazendo o 'git fetch --prune' para fazer a autodetecção do branch a ser enviado..."
	fi
	if [ -z "${confirma}" -o "${confirma,,}" = s ]; then
		echo "fazendo o git fetch --prune $REMOTE_REPO"
		git fetch --prune "$REMOTE_REPO"
		echo "terminou o git fetch --prune $REMOTE_REPO"
		declare -a BRANCH_EXISTS
		for i in `seq ${#BRANCH_LIFECYCLE[@]}`; do
			i=$(( i - 1 ))
			BRANCH_EXISTS[$i]=false
		done
		while read LINE; do
			branch_name=${LINE##*/}
			echo "julgando o branch $branch_name"
			for i in `seq ${#BRANCH_LIFECYCLE[@]}`; do
				i=$(( i - 1 ))
				branch_lifecycle_name="${BRANCH_LIFECYCLE[$i]}"
				if [ "$branch_name" = "$branch_lifecycle_name" ]; then
					echo "branch $branch_name existe no lifecycle"
					BRANCH_EXISTS[$i]=true
				fi
			done
		done < <(git branch --all | grep "remotes/$REMOTE_REPO/")
		achou_primeiro=false
		for i in `seq ${#BRANCH_LIFECYCLE[@]}`; do
			i=$(( i - 1 ))
			if ${BRANCH_EXISTS[$i]}; then
				branch_lifecycle_name="${BRANCH_LIFECYCLE[$i]}"
				if verifica_alem_futuro "$branch_lifecycle_name" $(( i + 1 )) ${#BRANCH_LIFECYCLE[@]}; then
					break
				fi
			fi
		done
	else
		echo "Saindo porque não se deseja fazer prune..."
		exit 1
	fi
elif [ -z "${SOURCE_SOURCE_BRANCH##*/*}" ]; then
	git fetch ${SOURCE_SOURCE_BRANCH%/*}
fi

if ${DO_DETECT_BRANCH}; then
	echo "Não foram detectadas mudanças"
	exit
fi

TITLE_MR=${SOURCE_PUSHED_BRANCH}

echo SOURCE_SOURCE_BRANCH $SOURCE_SOURCE_BRANCH
echo SOURCE_PUSHED_BRANCH $SOURCE_PUSHED_BRANCH
echo TARGET_BRANCH $TARGET_BRANCH
echo DO_DETECT_BRANCH $DO_DETECT_BRANCH
echo TITLE_MR $TITLE_MR

GITLAB_OPEN_MR_URL=${GITLAB_SERVER}/api/v4/projects/${PROJECT_ID}/merge_requests
GITLAB_OPEN_MR_PAYLOAD="
{
	\"id\":\"${PROJECT_ID}\",
	\"title\":\"${TITLE_MR}\",
	\"remove_source_branch\": true,
	\"source_branch\":\"${SOURCE_PUSHED_BRANCH}\",
	\"target_branch\":\"${TARGET_BRANCH}\"
}
"
GIT_ORIGIN_TARGET=+${SOURCE_SOURCE_BRANCH}:refs/heads/${SOURCE_PUSHED_BRANCH}


if ! ${DRY_RUN}; then
	git push $REMOTE_REPO ${GIT_ORIGIN_TARGET}
	curl ${GITLAB_OPEN_MR_URL} \
		--header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
		--header 'Content-Type: application/json' \
		--data "${GITLAB_OPEN_MR_PAYLOAD}"
else
	git push --dry-run $REMOTE_REPO ${GIT_ORIGIN_TARGET}

	echo "curl ${GITLAB_OPEN_MR_URL}, payload: '${GITLAB_OPEN_MR_PAYLOAD}'"
fi
