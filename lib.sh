#!/bin/bash

confirmacao() {
	local prompt resp
	prompt="$1"
	resp="[Yn]"

	read -p "${prompt:-Deseja confirmar?} $resp " leitura

	[ -z "$leitura" -o "${leitura^}" = Y ]
}

install_update_dirty_n_cheap() {
	declare -rg DIRTY_N_CHEAP_DIR=`git_basedir`/bin/.dirty-n-cheap

	if [ ! -d "$DIRTY_N_CHEAP_DIR" ]; then
		echo "clonando o dirty-n-cheap para poder usar o p2f..."
		git clone git@gitlab.com:geosales-open-source/dirty-n-cheap.git "$DIRTY_N_CHEAP_DIR"
	else
		(
			cd "$DIRTY_N_CHEAP_DIR"
			echo "dando pull para atualizar o dirty-n-cheap..."
			git pull
		)
	fi
}

git_basedir() {
	git rev-parse --show-toplevel 2>/dev/null | sed -E 's_^(.):_/\1/_'
}

# retorna verdadeiro se o valor estiver preenchido
verifica_var_preenchida() {
	[ -n "${!1}" ]
}

abort() {
	if [ $# -gt 0 ]; then
		echo "$@" >&2
	else
		echo "Abortando" >&2
	fi
	exit 1
}

abort_var_preenchida() {
	verifica_var_preenchida "$1" && abort "Variável '$1' já possui valor (${!1}), abortado" >&2
}
